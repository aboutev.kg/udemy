#FROM maven:3.9.6-eclipse-temurin-17 as build
#COPY . ./
#RUN mvn clean package -U -Dmaven.test.skip=true
#FROM openjdk:17-jdk-slim
#COPY --from=build  target/spring-boot-online-platform-0.0.1-SNAPSHOT.jar /udemy.jar
#ENTRYPOINT ["java", "-jar", "/udemy.jar"]